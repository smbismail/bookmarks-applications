import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Bookmark } from '../bookmark-interface';
import { BookmarkService } from '../bookmark.service';
import * as BookmarkActions from '../bookmark.actions';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-bookmark',
  templateUrl: './add-bookmark.component.html',
  styleUrls: ['./add-bookmark.component.css']
})
export class AddBookmarkComponent implements OnInit {

  bookmarkName = new FormControl('', [Validators.required]);
  bookmarkUrl = new FormControl('', [Validators.required]);
  bookmarkGroup = new FormControl('', [Validators.required]);


  constructor(private store: Store, private service: BookmarkService, private router: Router) { }

  groups = this.service.groups;

  ngOnInit() {
  }

  createBookmark() {
    let newBookmark: Bookmark = { id: '', name: '', url: '', group: '' };
    newBookmark.name = this.bookmarkName.value;
    newBookmark.url = this.bookmarkUrl.value;
    newBookmark.group = this.bookmarkGroup.value;

    this.store.dispatch(new BookmarkActions.CreateBookmark(newBookmark));
    this.router.navigate(['/']);
    
  }

}
