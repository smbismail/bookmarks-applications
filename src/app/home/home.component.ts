import { Component, OnInit } from '@angular/core';
import { BookmarkService } from '../bookmark.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  

  constructor(private service:BookmarkService) { }

  groups=this.service.groups;
  groupName = this.groups[0];

  ngOnInit() {
  }

}
