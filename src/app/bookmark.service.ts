import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bookmark } from './bookmark-interface';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  private url = 'http://localhost:3000/bookmarks';
  groups=['Travel', 'Health', 'Work', 'Personal']

  constructor(private http: HttpClient) { }

  getBookmarks(): Observable<Bookmark[]> {
    return this.http.get<Bookmark[]>(this.url)
  }

  deleteBookmark(bookmarkId): Observable<Bookmark>{
    return this.http.delete<Bookmark>(this.url+"/"+bookmarkId);
  }

  createBookmark(bookmark: Bookmark): Observable<Bookmark> {
    return this.http.post<Bookmark>(this.url, bookmark);
  }
}
